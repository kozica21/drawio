# Ideja projekta

1. Stranka želi motorski krmilnik z zahtevami:  

    ```
    - 1 BLDC motor 
    - vodenje navora in hitrosti 
    - 14-bitni absolutni enkoder 
    - USB vmesnik 
    - CAN vmesnik
    ```  

2. Na podlagi zahtev se ustvari konfiguracijska datoteka
3. Generacija rešitve
4. Avtomatska potrditev rešitve s pomočjo X2C Communicator-ja
5. Končna rešitev za stranko

# Prednosi MBD
- Uporaba že narejenih in testiranih modelov
- Že narejeni primeri uporabe na različnih krmilnikih
- Hitro testiranje in verifikacija delovanja
- Boljša preglednost projekta
- Zmanjšan čas razvoja


# Sestavni deli

### - Scilab/Xcos
- Pisanje algoritma
- Uporaba toolboxa X2C

### - X2C
- Generacija C kode
- Komunikacija s krmilnikom preko X2C Communicator-ja
- Real-time debugging s pomočjo X2C Scope (virtualni osciloskop)

### - HAL
- Abstraction layer med različnimi verzijami tiskanin, procesorjev, motorjev...

# Workflow
![Workflow diagram](Workflow_v4.png "workflow")

# MCU arhitektura
![MCU diagram](MCU_v3.png "MCU")


